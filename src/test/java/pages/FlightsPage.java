package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class FlightsPage {

    WebDriver driver;

    By changeDateCarrouselNext = By.xpath("//button[@data-e2e='carousel-next']");
    By departureDayUpdate = By.xpath("//span[text()='12']");
    By departureMonthUpdate = By.xpath("//span[text()='Oct']");
    By selectDepartureFlight = By.xpath("//flight-card[@data-e2e='flight-card--outbound'][1]");
    By destinationDayUpdate = By.xpath("//span[text()='24']");
    By destinationMonthUpdate = By.xpath("//span[text()='Oct']");
    By changeDateCarrouselPrevious = By.xpath("(//button[@data-e2e='carousel-prev'])[2]//carousel-arrow");
    By selectDestinationFlight = By.xpath("//flight-card[@data-e2e='flight-card--inbound'][1]");
   By valueFlightOption = By.xpath("//span[contains(text(),' Continue for')]");
    By continueValuePopup = By.xpath("//button[contains(text(),' Continue with Value fare')]");
    By loginLater = By.xpath("//span[contains(text(),'Log in later')]");
    By titleBtn = By.xpath("(//ry-dropdown)[1]");
    By titleBtn2 = By.xpath("(//ry-dropdown)[2]");
    By title1 = By.xpath("//div[text()='Mr']");
    By firstName1 = By.id("form.passengers.ADT-0.name");
    By lastName1 =  By.id("form.passengers.ADT-0.surname");
    By title2 = By.xpath("//div[text()='Mr']");
    By firstName2 = By.id("form.passengers.ADT-1.name");
    By lastName2 =  By.id("form.passengers.ADT-1.surname");
    By firstName3 = By.id("form.passengers.CHD-0.name");
    By lastName3 =  By.id("form.passengers.CHD-0.surname");
    By continueBtn =  By.xpath("//button[contains(text(),'Continue')]");
    By familySettingPopup = By.xpath("//button[contains(text(),'Okay, got it.')]");
    By seatDeparture1 = By.id("seat-20A");//20
    By seatDeparture2 = By.id("seat-20B");
    By seatDeparture3 = By.id("seat-20C");
    By seatDestination1 = By.id("seat-20A");
    By seatDestination2 = By.id("seat-20B");
    By seatDestination3 = By.id("seat-20C");
    By nextFlightBtn = By.xpath("//button[@data-ref='seats-action__button-next']");
    By noThanks = By.xpath("//button[contains(text(),'No, thanks')]");
    By smallBagRadioBtn = By.xpath("//p[contains(text(),'1 Small Bag only')]/../../preceding-sibling::div//input");



    public FlightsPage(WebDriver driver){
        this.driver=driver;

    }

    public void changeDates() {

        //changeDate(departureDayUpdate,departureMonthUpdate,"right",selectDepartureFlight);
        //changeDate(destinationDayUpdate,destinationMonthUpdate,"left",selectDestinationFlight);

        //Move right
        driver.findElement(changeDateCarrouselNext).click();

        //Select date to change it
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(departureDayUpdate));
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(departureDayUpdate));
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(departureDayUpdate).click();

        //Select flight
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(selectDepartureFlight));
        driver.findElement(selectDepartureFlight).click();

        //  Move left
        Actions act = new Actions(driver);
        WebElement prevArrow = driver.findElement(changeDateCarrouselPrevious);
        act.moveToElement(prevArrow);
        driver.findElement(changeDateCarrouselPrevious).click();

        //Select date to change it
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(destinationDayUpdate));
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(destinationDayUpdate));
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(destinationDayUpdate).click();

        //Select flight
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(selectDestinationFlight));
        driver.findElement(selectDestinationFlight).click();
    }

    public void changeDate(By dayChange, By monthChange, String direction, By flightType) {

         while(!elementPresent(monthChange)){

            carrouselMoveRightLeft(direction);
        }

         while(!elementPresent(dayChange)){

            carrouselMoveRightLeft(direction);

        }

        //Select date to change it
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(dayChange));
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(dayChange));
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(dayChange).click();

        //Select flight
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(flightType));
        driver.findElement(flightType).click();



    }


    public boolean elementPresent(By date){

        return driver.findElements(date).size()>0;

    }

    public void carrouselMoveRightLeft(String direction){

        if(direction.equals("right")){
            driver.findElement(changeDateCarrouselNext).click();
        }
        else{
            Actions act = new Actions(driver);
            WebElement prevArrow = driver.findElement(changeDateCarrouselPrevious);
            act.moveToElement(prevArrow);
            driver.findElement(changeDateCarrouselPrevious).click();

        }
    }

    public void selectValueOption() {

        //Select tarifa
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(valueFlightOption));
        driver.findElement(valueFlightOption).click();

        //Continue
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(continueValuePopup));
        driver.findElement(continueValuePopup).click();

        //Login later
        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginLater));
        driver.findElement(loginLater).click();

    }

    public void populatePassengersForm() throws InterruptedException {

        //Waiting time for the Title to appear and to be clickable
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(titleBtn));
        wait = new WebDriverWait(driver,20);
        driver.findElement(titleBtn).click();

/*
        // Configuring a FluentWait with polling interval and ignoring exception
        Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(4, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement firstTitle = wait1.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(title1);
            }
        });

        *
 */

        //Waiting time for the Title Mr option to appear and to be clickable
        wait = new WebDriverWait(driver,150);
        wait.until(ExpectedConditions.visibilityOfElementLocated(title1));
        wait.until(ExpectedConditions.elementToBeClickable(title1));

        //Only this sleep assures the element always be clicked
        Thread.sleep(1000);
        driver.findElement(title1).click();
        //driver.findElement(title1).click();
        driver.findElement(firstName1).sendKeys("Sónia");
        driver.findElement(lastName1).sendKeys("Pereira");
        driver.findElement(titleBtn2).click();
        driver.findElement(title2).click();
        driver.findElement(firstName2).sendKeys("Diogo");
        driver.findElement(lastName2).sendKeys("Bettencourt");
        driver.findElement(firstName3).sendKeys("Inês");
        driver.findElement(lastName3).sendKeys("Marçal");
        driver.findElement(continueBtn).click();

    }

    public void selectSeats() throws InterruptedException {

        //Loading the seats page takes a while
       //This waiting time is greater than other pages
        Thread.sleep(6000);

        //Closing the popup
        driver.findElement(familySettingPopup).click();

        //Selecting seats
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.elementToBeClickable(seatDeparture1));

        driver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);
        driver.findElement(seatDeparture1).click();

        wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.elementToBeClickable(seatDeparture2));

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(seatDeparture2).click();

        wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.elementToBeClickable(seatDeparture3));

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(seatDeparture3).click();
        driver.findElement(nextFlightBtn).click();

        //Sometimes nextFlight button takes longer to show the next screen
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)", "");

        wait = new WebDriverWait(driver,40);
        wait.until(ExpectedConditions.visibilityOfElementLocated(seatDestination1));

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.findElement(seatDestination1).click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(seatDestination2).click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(seatDestination3).click();

        driver.findElement(continueBtn).click();

    }
public void selectPackage() throws InterruptedException {

        //Loading the seats page takes a while
       //This waiting time is greater than other pages
        Thread.sleep(4000);

        WebDriverWait wait = new WebDriverWait(driver,100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(noThanks));
        driver.findElement(noThanks).click();

        //Loading the seats page takes a while
        //This waiting time is greater than other pages
        Thread.sleep(7000);

        //Scroll until the element and click the element
        WebElement  radioBtn = driver.findElement(smallBagRadioBtn);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", radioBtn);

        wait = new WebDriverWait(driver,100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(continueBtn));

        //Scroll to the view of the element continue, on the bottom of the page
        WebElement  continueLastPageBtn = driver.findElement(continueBtn);
        js.executeScript("arguments[0].scrollIntoView();", continueLastPageBtn);
        continueLastPageBtn.click();



    }


}
